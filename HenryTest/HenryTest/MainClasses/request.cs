using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Net;
using System.ServiceModel;
using System.Runtime.Serialization.Json;
using System.IO;

namespace HenryTest.MainClasses{

    public class Request{


        public static HttpWebRequest GetTextPlainReqObj(string path) {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);
            request.Timeout = 10000;
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;
            request.ContentType = "text/plain";
            request.Method = "POST";

            return request;
        }

        public static HttpWebRequest GetHttpPostRequest(string route) {

            var request = (HttpWebRequest)WebRequest.Create(route);
            request.Timeout = 10000;
            request.KeepAlive = true;
            request.ProtocolVersion = HttpVersion.Version10;
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ServicePoint.Expect100Continue = false;

            return request;
        }

        public static HttpWebRequest GetHttpGetRequest(string route) {

            var request = (HttpWebRequest)WebRequest.Create(route);
            request.Timeout = 10000;
            request.KeepAlive = true;
            request.ProtocolVersion = HttpVersion.Version10;
            request.Method = "GET";
            request.ServicePoint.Expect100Continue = false;

            return request;
        }

        public static T GetObjectFromJsonString<T>(T obj, string receive) {

            try {

                T objResp;
                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(T));

                using (MemoryStream ms = new MemoryStream()) {
                    byte[] bt = UTF8Encoding.UTF8.GetBytes(receive);
                    ms.Write(bt, 0, bt.Length);
                    ms.Position = 0;
                    objResp = (T)deserializer.ReadObject(ms);
                }

                return objResp;
            }
            catch(FormatException fe){
                LogWriter.WriteLog2("Exceção ao processar requisição: " + fe.Message);
                return (T)new object();
            }
            catch(Exception e) {
                LogWriter.WriteLog2("Exceção ao processar requisição: " + e.Message);
                return (T)new object();
            }

        }

        // Processar a stream e retornar o objeto do tipo T
        public static T GetResponseObject<T>(T obj, WebResponse response)
        {

            try
            {

                string receive;
                T objResp;
                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(T));

                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    receive = sr.ReadToEnd();
                using (MemoryStream ms = new MemoryStream())
                {
                    byte[] bt = UTF8Encoding.UTF8.GetBytes(receive);
                    ms.Write(bt, 0, bt.Length);
                    ms.Position = 0;
                    objResp = (T)deserializer.ReadObject(ms);
                }

                return objResp;
            }
            catch (FormatException fe)
            {
                LogWriter.WriteLog2("Exceção ao processar requisição: " + fe.Message);
                return (T)new object();
            }
            catch (Exception e)
            {
                LogWriter.WriteLog2("Exceção ao processar requisição: " + e.Message);
                return (T)new object();
            }

        }
    }
}

