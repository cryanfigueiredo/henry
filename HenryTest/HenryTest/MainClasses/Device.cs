﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HenryTest.MainClasses
{
    public class Device
    {
        public string ip = "";
        public int port;
        public long id;
        public long DatabaseId;
        public string descricao;
        public List<Reader> readers;

        public System.Net.Sockets.TcpClient client;

        public Atuador atuadorInfo = new Atuador();

        public object Clone()
        {
            return CloneDevice();
        }

        protected Device CloneDevice()
        {
            var copy = (Device)MemberwiseClone();
            return copy;
        }
    }
}
