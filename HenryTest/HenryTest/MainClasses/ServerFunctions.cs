using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;
using System.Globalization;
using System.Configuration;
using System.Timers;
using Newtonsoft.Json.Linq;
using HenryTest.Henry8X;

namespace HenryTest.MainClasses{

	public class ServerFunctions{

		public string AuthorityServerUrl = "http://localhost:8082/EvofluxBusinessWS/business/";
        public List<Device> OnListDevices;
        public List<Device> offListDevices;
        public int DevicesDefaultPort = 3000;

        LogWriter logWriter;

        public ServerFunctions()
        {
            logWriter = new LogWriter();
        }

        public string AuthorityServer {

            get { return AuthorityServerUrl; }

            set {

                if (!value[value.Length - 1].ToString().Equals("/")) {
                    AuthorityServerUrl = value + "/";
                }
                else {
                    AuthorityServerUrl = value;
                }
            }
        }

		public ApiUserMessageObjectResponse MainAcessAuthentication(string card, long device, int readerId){

            ApiUserMessageObjectResponse user = null;

            logWriter.WriteLog("Passagem de usuario...");

            try {

                string comandoEntrada = "I";
                object objResponse = null;
                string inputUsuario = "{\"idCartao\":" + card + ",\"atuador\":" + device
                + " ,\"comandoEntrada\":" + "\"" + comandoEntrada + "\"" + ",\"readerId\":" + readerId
                + ",\"messages\":[]" + ",\"isBiometria\":\" false" + "\"}";

                var request = Request.GetHttpPostRequest("usuarioauthentication");

                logWriter.WriteLog(" JSON de requisição a autenticação de usuário -> " + inputUsuario);

                using (Stream send = request.GetRequestStream()) {

                    var userData = Encoding.UTF8.GetBytes(inputUsuario);

                    send.Write(userData, 0, userData.Length);

                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {

                    if (response.StatusCode == HttpStatusCode.OK) {

                        //ShowMessageOnLogBox("Acesso por cartão liberado => Cartão: " + num);

                        DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(ApiUserResponse));

                        ApiUserResponse result = (ApiUserResponse)deserializer.ReadObject(response.GetResponseStream());


                        if (result != null &&
                            result.objectResponse != null) {

                            user = result.objectResponse.mensagem;

                        }
                    }
                    else {

                        logWriter.WriteLog("Acesso por cartao negado => Cartao : " + card);

                        //ShowMessageOnLogBox("Acesso por cartão negado => Cartão: " + num);
                    }
                }

            }
            catch (Exception we) {
                
                //ShowMessageOnLogBox("Exceção na autenticação de identificador: " + num + ". Dispositivo: " + args.Msg.DeviceID.ToString() + ". Reader: " + readerId + "\n" + we.Message);
                
                logWriter.WriteLog("Exceção na autenticação de identificador: " + card + ". Dispositivo: " + device + ". Reader: " + readerId + "\n" + we.Message + " -> " + we.StackTrace);

            }

            return user;
		}

        public List<Reader> FetchReadersFromServer(long id) {

            List<Reader> readers = new List<Reader>();

            try {

                HttpWebRequest request = Request.GetHttpGetRequest( AuthorityServerUrl + "fetch_readers_from_device/" + id);

                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(List<Reader>));

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {

                    using (StreamReader sr = new StreamReader(response.GetResponseStream())) {

                        string receive = sr.ReadToEnd();

                        using (MemoryStream ms = new MemoryStream()) {

                            byte[] bt = UTF8Encoding.UTF8.GetBytes(receive);
                            ms.Write(bt, 0, bt.Length);
                            ms.Position = 0;
                            readers = (List<Reader>)deserializer.ReadObject(ms);

                        }
                    }
                }
            }
            catch (WebException e) {
                logWriter.WriteLog(" WebException while fetching device" + id + " readers information from webservice -> " + e.Message);
            }
            catch (Exception e) {
                logWriter.WriteLog(" Exception while getting readers information for device " + id + " -> " + e.Message);
            }

            return readers;
        }

        // Informações sobre um atuador específico...
        public Atuador GetAtuadorInfo(long id) {

            Atuador result = new Atuador();

                try {

                    HttpWebRequest request = Request.GetTextPlainReqObj(AuthorityServerUrl + "atuador");

                    // Requisição para o WebService
                    using (StreamWriter send = new StreamWriter(request.GetRequestStream())) {

                        string idstr = id.ToString();

                        send.WriteLine(idstr);
                    }

                    using (WebResponse response = request.GetResponse()) {
                        
                        ApiAtuadorResponse obj =  new ApiAtuadorResponse();

                        obj = Request.GetResponseObject(obj, response); 

                        if (obj.isRequestValid) {

                            result.id = id;
                                
                            // O objeto é enviado como uma string JSON scaped
                            // Por isso extrai-se os valores com Regex                              
                            string res = obj.objectResponse.mensagem;

                            result = Request.GetObjectFromJsonString(result, res);

                        }
                    }

                    return result;
                }
                catch (WebException e) {
                   //ShowMessageOnLogBox("Execeção ao buscar informações do atuador " + id +" :" + e.Message);
                    return result;
                

            }catch(Exception ex) {
                //ShowMessageOnLogBox("Execeção ao buscar informações do atuador " + id + " :" + ex.Message);
                return result;
            }

                
        }

        public List<Atuador> AtuadoresFromServer() {

            List<Atuador> list = new List<Atuador>();

            string whereClause = "fabricante_id = 10";

            try {

                HttpWebRequest request = Request.GetHttpPostRequest(AuthorityServerUrl + "buscaratuadoremployes");

                UsersListRequestObj obj = new UsersListRequestObj() { condicaoWhere = whereClause };

                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(UsersListRequestObj));

                // Requisição para o WebService
                using (Stream send = request.GetRequestStream()) {
                    serializer.WriteObject(send, obj);
                }

                using (WebResponse response = request.GetResponse()) {

                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(ApiListAtuadoresResponse));

                    using (StreamReader sr = new StreamReader(response.GetResponseStream())) {

                        string receive = sr.ReadToEnd();

                        using (MemoryStream ms = new MemoryStream()) {

                            byte[] bt = UTF8Encoding.UTF8.GetBytes(receive);
                            ms.Write(bt, 0, bt.Length);
                            ms.Position = 0;

                                try {

                                    ApiListAtuadoresResponse res = (ApiListAtuadoresResponse)deserializer.ReadObject(ms);

                                    if (res.isRequestValid) {
                                        list.AddRange(res.objectResponse.mensagem);
                                    }
                                }
                                catch(Exception e) {
                                    logWriter.WriteLog("Exceção na serialização de dados dos atuadores: " + e.Message);
                                    //IncomingMessage("Exceção na serialização de dados dos atuadores: " + e.Message);
                                    return list;
                                }

                        }
                    }

                }
            }
            catch (WebException e) {

                logWriter.WriteLog("Erro de conexão com o servidor na consulta aos atuadores: " + e.Message);              
                //IncomingMessage("Verifique a conexão com o servidor, erro ao tentar buscar atuadores");
                
             }

            catch (Exception e) {

                logWriter.WriteLog("Erro de conexão com o servidor na consulta aos atuadores: " + e.Message);              
                //IncomingMessage("Verifique a conexão com o servidor, erro ao tentar buscar atuadores");
                
             }

            return list;       
        }

        public void ReceiveAtuadoresInfo(List<Device> clients)
        {

            Task.Run(() =>
            {

                //IncomingMessage("\nA Receber Informações dos Atuadores pelo WebService...\n");

                List<Atuador> list = AtuadoresFromServer();

                if (list.Count > 0)
                {

                    Device aux = null;

                    foreach (Atuador atuador in list)
                    {

                        if (clients.Count > 0)
                        {


                            if (clients.FindIndex( x => x.id == atuador.idatuadordgc) > -1)
                            {

                                aux = clients.FirstOrDefault((x) => x.id == atuador.idatuadordgc);
                                aux.atuadorInfo = atuador;

                                //IncomingMessage("\nInformações do Atuador: " + atuador.descricao + "Recebidas...\n");
                            }
                        }
                        else
                        {

                            if (Utils.IsValidUrl(atuador.enderecoip))
                            {

                                // ############# CONECTAR O DISPOSITIVO 
                                 ConnectAndSetOnlineDevice(atuador);

                            }

                        }

                    }

                    
                    //IncomingMessage("\n Recebidas as Informações de " + list.Count + " Disposistivos...\n\n");
                }
                else
                {
                    
                    //IncomingMessage("\n A Consulta Retornou um Array Vazio...\n");
                }
            });

        }

        public void ConnectAndSetOnlineDevice(Atuador atuador)
        {
            try
            {
                bool result = Utils.DeviceIsOnTheNet(atuador.enderecoip);

                if (result)
                {
                    if (ExistsClientInTheServer(atuador.enderecoip))
                    {

                        Device resul = OnListDevices.FirstOrDefault(x => x.ip.Equals(atuador.enderecoip));
                        bool conectionSuccess = false;

                        for (int i = 0; i < 3; i++)
                        {
                            if (TryConnect(resul))
                            {
                                conectionSuccess = true;
                                break;
                            }
                        }

                        if (conectionSuccess)
                        {
                            // Inicia escuta de eventos do dispositivo
                            BuildWhileInfiniteTask(resul);
                        }
                        else
                        {
                            int index = OnListDevices.FindIndex(x => x.id == resul.id);
                            resul.client = null;

                            // Adicionando a lista de offlines
                            offListDevices.Add((Device)resul.Clone());
                            logWriter.WriteLog("Dispositivo " + resul.id + " adicionado à lista de offlines..");

                            // Removendo da lista online...
                            OnListDevices.RemoveAt(index);
                            logWriter.WriteLog("Dispositivo " + resul.id + " removido da lista de onlines...");
                        }

                    }
                    else
                    {
                        Device dev = new Device()
                        {
                            ip = atuador.enderecoip,
                            id = atuador.idatuadordgc,
                            DatabaseId = atuador.id,
                            port = DevicesDefaultPort,
                            descricao = atuador.descricao,
                            atuadorInfo = atuador
                        };
                        bool conectionSuccess = false;

                        for (int i = 0; i < 3; i++)
                        {
                            if(TryConnect(dev))
                            {
                                conectionSuccess = true;
                                break;
                            }
                        }

                        if (conectionSuccess)
                        {
                            dev.readers = FetchReadersFromServer(dev.id);
                            BuildWhileInfiniteTask(dev);
                        }
                        else
                        {
                            
                            offListDevices.Add(dev);
                            logWriter.WriteLog("Dispositivo " + dev.id + " adicionado à lista de offlines..");
                        }
                        
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        public bool TryConnect(Device device)
        {
            try
            {
                device.client = new TcpClient(device.ip, device.port);

                return true;
            }
            catch(SocketException ex)
            {
                logWriter.WriteLog("Conexão falhou para o dispositivo " + device.ip);
                return false;
            }
            catch(Exception ex)
            {
                logWriter.WriteLog("Conexão falhou para o dispositivo " + device.ip);
                return false;
            }
        }

        public void BuildWhileInfiniteTask(Device device)
        {
            Task.Run(() =>
            {

                try
                {

                    NetworkStream stream = device.client.GetStream();

                    while (device.client.Connected)
                    {

                        if (device.client.Available > 0 && stream.CanRead)
                        {
                            byte[] myReadBuffer = new byte[1024];
                            StringBuilder myCompleteMessage = new StringBuilder();
                            int numberOfBytesRead = 0;
                            string mess = String.Empty;
                            // Incoming message may be larger than the buffer size.
                            do
                            {
                                numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length);

                                mess = Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead);

                                myCompleteMessage.AppendFormat("{0}", mess);


                            }
                            while (stream.DataAvailable);

                            if (myCompleteMessage.Length > 0)
                            {
                                
                                ObjetoRequest obj = Utils.GetTransformMsgToObj(mess);

                                stream.Write();

                                // Print out the received message to the console.
                                Console.WriteLine("You received the following message : " +
                                                                myCompleteMessage.ToString());
                            }
                        }
                    }

                    stream.Close();
                    device.client.Close();
                }
                catch(SocketException se)
                {

                }catch(Exception e)
                {

                }
            });
        }

        public bool ExistsClientInTheServer(string ip)
        {
            return OnListDevices.FirstOrDefault(x => x.ip.Equals(ip)) != null;
        }

        public bool WebServiceIsAlive() {

            bool yes = true;

            try {

                HttpWebRequest request = Request.GetHttpGetRequest(AuthorityServerUrl + "deploy-success");

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {

                    if (response.StatusCode == HttpStatusCode.OK) {
                        return yes;
                    }
                }
            }
            catch (Exception e) {
                yes = false;
                return yes;
            }

            return yes;
        }

        // Função para verificação da licença ao subir o servidor
        // Uma primeira checagem
        public void StartingLicenseCheck() {

            bool result = CheckApplicationLicense();

            if (!result) {

                logWriter.WriteLog("Chave de Licença Inválida ou Expirada\n A Aplicação será Fechada...");

                //CloseLikeCrash();
            }
        }
        

        public bool CheckApplicationLicense() {

            bool isValid = false;

            try {

                HttpWebRequest request = Request.GetHttpGetRequest(AuthorityServerUrl + "checarchave");

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {

                    bool ok = response.StatusCode == HttpStatusCode.OK;
                    Type reqType = ok ? typeof(ApiResponse2) : typeof(ApiResponse);

                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(reqType);

                    using (StreamReader sr = new StreamReader(response.GetResponseStream())) {

                        string receive = sr.ReadToEnd();

                        using (MemoryStream ms = new MemoryStream()) {

                            byte[] bt = UTF8Encoding.UTF8.GetBytes(receive);
                            ms.Write(bt, 0, bt.Length);
                            ms.Position = 0;

                            if (ok) {
                                ApiResponse2 obj = (ApiResponse2)deserializer.ReadObject(ms);
                                isValid = obj.objectResponse.mensagem;
                            }
                            else {
                                ApiResponse obj = (ApiResponse)deserializer.ReadObject(ms);
                                isValid = obj.message.mensagem.ToLower().Contains("true");
                            }

                        }
                    }
                }

                return isValid;
            }
            catch (WebException ex) {
                logWriter.WriteLog("Exceção ao consultar licença");
                logWriter.WriteLog(ex.Message);
                return false;
            }
            catch (Exception er) {
                logWriter.WriteLog("Exceção ao consultar licença");
                logWriter.WriteLog(er.Message);
                logWriter.WriteLog(er.StackTrace);
                return false;
            }

        }

        // Trazendo informações da configuração do sistema
        public ConfigSystem GetSystemConfiguration() {

            ConfigSystem conf = new ConfigSystem();

            try {

                HttpWebRequest request = Request.GetHttpGetRequest(AuthorityServerUrl + "config");

                using (WebResponse response = request.GetResponse()) {

                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(ConfigSystemReq));

                    using (StreamReader sr = new StreamReader(response.GetResponseStream())) {

                        string receive = sr.ReadToEnd();

                        using (MemoryStream ms = new MemoryStream()) {

                            byte[] bt = UTF8Encoding.UTF8.GetBytes(receive);
                            ms.Write(bt, 0, bt.Length);
                            ms.Position = 0;
                            ConfigSystemReq obj = (ConfigSystemReq)deserializer.ReadObject(ms);

                            if (obj.isRequestValid) {

                                conf = obj.objectResponse.mensagem;

                            }
                            else {
                                logWriter.WriteLog("Erro ao buscar as configurações do sistema no servidor");

                                //ShowMessageOnLogBox("Erro ao buscar as configurações do sistema no servidor");

                            }
                        }
                    }

                    return conf;
                }
            }
            catch (WebException e) {

                logWriter.WriteLog("Exceção ao tentar buscar as configurações do sistema no servidor");
                logWriter.WriteLog(e.Message);

                //ShowMessageOnLogBox("Busca de Configurações do sistema. Exceção: " + e.Message);


                return conf;
            }
        }

        public void RequestBaixaAutomatica() {

            try {

                var requisicaoWeb = Request.GetHttpGetRequest(AuthorityServerUrl + "executarsaidaautometica");
                requisicaoWeb.UserAgent = "RequisicaoWebDemo";

                using (var resposta = requisicaoWeb.GetResponse()) {
                    var streamDados = resposta.GetResponseStream();
                    StreamReader reader = new StreamReader(streamDados);
                    object objResponse = reader.ReadToEnd();
                    JObject parsed = JObject.Parse(objResponse.ToString());
                    streamDados.Close();
                    resposta.Close();

                    foreach (var parse in parsed) {
                        // string teste2 = parse.Value.ToString();
                        if (parse.Value.ToString().Equals("True")) {
                            //ShowMessageOnLogBox("Baixa automática de visitantes realizada com sucesso!");
                            logWriter.WriteLog("Erro na baixa automática de visitantes.");
                            break;
                        }
                        else {
                            //ShowMessageOnLogBox("Erro na baixa automática de visitantes!");
                            break;
                        }
                    }
                }

            }
            catch (Exception we) {
                //ShowMessageOnLogBox(we.Message);
                logWriter.WriteLog("Exceção na requisição de baixa automática." + "\n" + we.Message);
            }


        }

        public void SaveWebServiceServerUrl(string url) {

            AuthorityServer = url;

            //Request.AuthorityServerUrl = AuthorityServerUrl;

            //Load appsettings
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var settings = config.AppSettings.Settings;

            if (settings["ServerUrl"] != null) {
                settings.Remove("ServerUrl");
            }

            settings.Add("ServerUrl", url);

            config.Save(ConfigurationSaveMode.Modified);

            ConfigurationManager.RefreshSection("appSettings");

        }

        public void SaveDefaultDevicesPort(int port)
        {

            //Load appsettings
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var settings = config.AppSettings.Settings;

            if (settings["DevicesPort"] != null)
            {
                settings.Remove("DevicesPort");
            }

            settings.Add("DevicesPort", port.ToString());

            config.Save(ConfigurationSaveMode.Modified);

            ConfigurationManager.RefreshSection("appSettings");

        }



        public static TimeSpan BrHourTimeSpan(ref string hourVar) {

            if (String.IsNullOrEmpty(hourVar)) { hourVar = "ERRO"; return TimeSpan.Zero; }

            string brDatePattern = "^[0-9]{2}\\:[0-9]{2}";

            if (Regex.IsMatch(hourVar, brDatePattern)) {

                int HourDivisor = Regex.Match(hourVar, @":").Index;

                int hour = Int32.Parse(hourVar.Substring(0, (hourVar.Length - 1) - HourDivisor).Trim());
                int minute = Int32.Parse(hourVar.Substring(HourDivisor + 1).Trim());

                TimeSpan date = new TimeSpan(hour, minute, 0);

                return date;
            }
            else {
                hourVar = "ERRO";
            }

            return TimeSpan.Zero;
        }

        public string[] savedLoadTimes(int type) {

            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            string[] hours = new string[2];

            //Thread de biometrias
            if (type == 0) {

                if (config.AppSettings.Settings["hourbeginloademp"] != null) {

                    string start = config.AppSettings.Settings["hourbeginloademp"].Value.ToString();
                    string end = config.AppSettings.Settings["hourendloademp"].Value.ToString();

                    hours[0] = start;
                    hours[1] = end;
                }
            }
            else {

                if (config.AppSettings.Settings["hourlowautomvisitorbegin"] != null) {

                    string start = config.AppSettings.Settings["hourlowautomvisitorbegin"].Value.ToString();
                    string end = config.AppSettings.Settings["hourlowautomvisitorend"].Value.ToString();

                    hours[0] = start;
                    hours[1] = end;
                }
            }



            return hours;
        }

        public void SaveTimes(string start, string end, int type) {

            //Load appsettings
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var settings = config.AppSettings.Settings;

            // Thread de biometrias
            if (type == 0) {

                if (settings["hourbeginloademp"] != null) {
                    settings.Remove("hourbeginloademp");
                }
                if (settings["hourendloademp"] != null) {
                    settings.Remove("hourendloademp");
                }

                settings.Add("hourbeginloademp", start);
                settings.Add("hourendloademp", end);
            }
            else {

                if (settings["hourlowautomvisitorbegin"] != null) {
                    settings.Remove("hourlowautomvisitorbegin");
                }
                if (settings["hourlowautomvisitorend"] != null) {
                    settings.Remove("hourlowautomvisitorend");
                }

                settings.Add("hourlowautomvisitorbegin", start);
                settings.Add("hourlowautomvisitorend", end);
            }


            config.Save(ConfigurationSaveMode.Modified);

            ConfigurationManager.RefreshSection("appSettings");
        }

        // Função assíncrona para registro de acesso negado
        public void SendAccessDenied(long device_id, string card, int reader)
        {

            Task.Run(() => {

                try
                {

                    var request = Request.GetHttpPostRequest("acessonegado");
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ApiUsuarioReaderObject));

                    ApiUsuarioReaderObject access = new ApiUsuarioReaderObject();
                    
                    access.idDevice = device_id > 0 ? device_id.ToString() : "";
                    
                    access.dataEvento = DateTime.Now.Date.ToString("d", CultureInfo.CreateSpecificCulture("pt-BR"));
                    access.horaEvento = DateTime.Now.TimeOfDay.ToString("t", CultureInfo.CreateSpecificCulture("pt-BR")).Substring(0, 8);

                    access.messages = "Usário Negado";

                    string message = String.Format("Usuário Negado no Dispositivo ID:  {0} - Cartao numero: {1} - Às {2} de {3} - Reader ID: {4}",
                        access.idDevice, access.idCard, access.horaEvento, access.dataEvento, access.readerId);

                    logWriter.WriteLog(message);

                    using (Stream send = request.GetRequestStream())
                    {
                        serializer.WriteObject(send, access);
                    }

                    using (WebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        if (String.IsNullOrEmpty(response.ContentType))
                        {
                            logWriter.WriteLog("Acesso Negado No Dispositivo: " + device_id + " Salvo No Servidor");
                        }
                    }

                }
                catch (WebException e)
                {
                    //ShowMessageOnLogBox(" Webexception lançada durante o envio de acesso negado -> " + e.Message + " -> " + e.StackTrace);
                    logWriter.WriteLog(" Webexception lançada durante o envio de acesso negado -> " + e.Message + " -> " + e.StackTrace);
                }
                catch (Exception ee)
                {
                    logWriter.WriteLog(" Webexception lançada durante o envio de acesso negado -> " + ee.Message + " -> " + ee.StackTrace);
                }

            });


        }

        //acesso permitido
        public void SendAccessGranted(long device_id, string card, int reader)
        {

            Task.Run(() => {

                Device result = GetClientByID(mac);

                if (result == null)
                {
                    Console.WriteLine("Logs de Acesso Não Enviados...");
                    logWriter.WriteLog("Logs De Acesso Não Eviados ao WebService");
                    return;
                }

                try
                {

                    var request = Request.GetHttpPostRequest("acessopermitido");
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ApiAcessGrantedObject));

                    // Construindo o objeto a ser enviado na requisição
                    ApiAcessGrantedObject access = new ApiAcessGrantedObject();

                    Device deviceAux = CheckExistence(result.id);

                    SimpleAcessAddParams addParams = new SimpleAcessAddParams();
                    addParams.interfaceId = pars[0];
                    addParams.readerAddress = pars[1];
                    addParams.uniqueid = pars[2];

                    access.atuador = result.id > 0 ? result.id.ToString() : "";
                    access.idDevice = access.atuador;
                    access.dataEvento = DateTime.Now.Date.ToString("d", CultureInfo.CreateSpecificCulture("pt-BR"));
                    access.horaEvento = DateTime.Now.ToString("HH:mm:ss", CultureInfo.CreateSpecificCulture("pt-BR"));

                    string device_id = result.id.ToString();
                    int read = Int32.Parse(addParams.readerAddress) + 1;

                    // Informações guardadas temporariamente no momento da tentativa de acesso
                    if (cache.ContainsKey(device_id))
                    {

                        string card_value = cache[device_id].card_value;
                        access.idCard = !String.IsNullOrEmpty(card_value) ? card_value.ToString() : "0";

                        // tipo do leitor (reader)
                        long deviceCode = cache[device_id].identifier_id;
                        access.readerId = 1;

                        Reader rd2 = deviceAux.readers.FirstOrDefault(x => x.codigo == read);

                        if (rd2 != null)
                        {
                            access.readerId = rd2.codigo;
                            access.direcao = rd2.sentido;
                        }
                        else
                        {
                            access.direcao = Int32.Parse(addParams.readerAddress) == 0 ? 1 : 2;
                        }

                        if (cache[device_id].user_id > 0)
                        {
                            access.idUsuario = cache[device_id].user_id.ToString();
                            access.idCard = access.idUsuario;
                        }
                    }
                    else
                    {
                        access.idCard = "0";
                        access.readerId = 0;
                        access.idUsuario = "0";
                        access.direcao = 1;
                    }

                    // Apagando as informações temporárias
                    cache.Remove(access.idDevice);

                    access.messages = "Abertura de Porta no Dispositivo: " + access.idDevice;

                    // Requisição para o WebService
                    using (Stream send = request.GetRequestStream())
                    {
                        serializer.WriteObject(send, access);
                    }

                    using (WebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        if (String.IsNullOrEmpty(response.ContentType))
                        {
                            logWriter.WriteLog("Acesso Pelo Dispositivo: " + access.idDevice + " Salvo No Servidor");
                        }
                    }

                }
                catch (WebException e)
                {
                    logWriter.WriteLog(" exception lançada no envio de acesso permitido -> " + e.Message + " -> " + e.StackTrace);
                }

            });
        }

        public List<Device> CheckAliveDevices(List<Device> clients)
        {

            List<Device> offlineDevices = new List<Device>();

            foreach (Device dev in clients)
            {

                string ip = dev.ip;

                if (!Utils.DeviceIsOnTheNet(ip))
                {
                    offlineDevices.Add((Device)dev.Clone());
                }
            }

            return offlineDevices;
        }

        /* CARGA */

        // public Dictionary<String, HashSet<Usuario>> GetLoad() {

        //     Dictionary<String, HashSet<Usuario>> totalLoad = new Dictionary<string, HashSet<Usuario>>();

        //     try {

        //         HttpWebRequest request = Request.GetHttpGetRequest("load/" + fabricanteId);

        //         using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {

        //             DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(LoadResponse));

        //             using (StreamReader sr = new StreamReader(response.GetResponseStream())) {

        //                 string receive = sr.ReadToEnd();

        //                 using (MemoryStream ms = new MemoryStream()) {

        //                     byte[] bt = UTF8Encoding.UTF8.GetBytes(receive);
        //                     ms.Write(bt, 0, bt.Length);
        //                     ms.Position = 0;
        //                     LoadResponse obj = (LoadResponse)deserializer.ReadObject(ms);

        //                     if (obj.isRequestValid) {

        //                         LoadObjectResponseMessage result = obj.objectResponse.mensagem;

        //                         //if (result.BIOMETRIA != null) {

        //                         //    HashSet<Usuario> ha = new HashSet<Usuario>();

        //                         //    foreach (LoadBioObj lo in result.BIOMETRIA) {

        //                         //        ha.Add(new Usuario() {

        //                         //            id = (int)lo.cod_usr,
        //                         //            passwd = lo.passwd,
        //                         //            tagveicular = lo.tagVeicular,
        //                         //            identificador = lo.identificador,
        //                         //            atuadoresIdsList = lo.atuadoresIdsList,
        //                         //            templates = new List<Template>() {
        //                         //                    new Template() {
        //                         //                        tipoBiometria = 0,
        //                         //                        digital = lo.template
        //                         //                    },
        //                         //                    new Template() {
        //                         //                        tipoBiometria = 0,
        //                         //                        digital = lo.template2
        //                         //                    }
        //                         //                }

        //                         //        });
        //                         //    }

        //                         //    totalLoad.Add("BIOMETRIA", ha);
        //                         //}
        //                         if (result.CARTAO != null) {

        //                             HashSet<Usuario> ha = new HashSet<Usuario>();

        //                             foreach (LoadBioObj lo in result.CARTAO) {

        //                                 ha.Add(new Usuario() {

        //                                     id = (int)lo.cod_usr,
        //                                     passwd = lo.passwd,
        //                                     tagveicular = lo.tagVeicular,
        //                                     identificador = lo.identificador,
        //                                     atuadoresIdsList = lo.atuadoresIdsList

        //                                 });
        //                             }

        //                             totalLoad.Add("CARTAO", ha);
        //                         }
        //                         //if (result.TAGVEICULAR != null) {

        //                         //    HashSet<Usuario> ha = new HashSet<Usuario>();

        //                         //    foreach (LoadBioObj lo in result.TAGVEICULAR) {

        //                         //        ha.Add(new Usuario() {

        //                         //            id = (int)lo.cod_usr,
        //                         //            passwd = lo.passwd,
        //                         //            tagveicular = lo.tagVeicular,
        //                         //            identificador = lo.identificador,
        //                         //            atuadoresIdsList = lo.atuadoresIdsList

        //                         //        });
        //                         //    }

        //                         //    totalLoad.Add("TAGVEICULAR", ha);
        //                         //}
        //                         //if (result.TECLADO != null) {

        //                         //    HashSet<Usuario> ha = new HashSet<Usuario>();

        //                         //    foreach (LoadBioObj lo in result.TECLADO) {

        //                         //        ha.Add(new Usuario() {

        //                         //            id = (int)lo.cod_usr,
        //                         //            passwd = lo.passwd,
        //                         //            tagveicular = lo.tagVeicular,
        //                         //            identificador = lo.identificador,
        //                         //            atuadoresIdsList = lo.atuadoresIdsList,
        //                         //            templates = new List<Template>() {
        //                         //                    new Template() {
        //                         //                        tipoBiometria = 0,
        //                         //                        digital = lo.template
        //                         //                    }
        //                         //                }

        //                         //        });
        //                         //    }

        //                         //    totalLoad.Add("TECLADO", ha);
        //                         //}

        //                     }
        //                     else {

        //                         logWriter.WriteLog("Erro ao buscar a carga para os dispositivos no servidor");

        //                         //ShowMessageOnLogBox("Erro ao buscar a carga para os dispositivos no servidor");

        //                     }
        //                 }
        //             }

        //         }


        //     }
        //     catch (Exception we) {
        //         logWriter.WriteLog("Erro ao buscar a carga para os dispositivos no servidor \n" + we.Message);
        //         //ShowMessageOnLogBox("Erro ao buscar a carga para os dispositivos no servidor \n" + we.Message);

        //     }

        //     return totalLoad;
        // }
    }
}
    