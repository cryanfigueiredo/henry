using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Net;
using System.ServiceModel;
using System.Runtime.Serialization.Json;
using System.IO;

namespace HenryTest.MainClasses{


    [DataContract]
    public class Atuador {

        [DataMember(EmitDefaultValue = false)]
        public long hashCode = 0;
        [DataMember(EmitDefaultValue = false)]
        public long id = 0;
        [DataMember(EmitDefaultValue = false)]
        public int idtipoatuador = 1;
        [DataMember(EmitDefaultValue = false)]
        public string descricao = "Atuador";
        [DataMember(EmitDefaultValue = false)]
        public string enderecoip = "";
        [DataMember]
        public string enderecoipfilho;
        [DataMember]
        public bool flativo;
        [DataMember]
        public string reffisica;
        [DataMember]
        public string movimentoentrar;
        [DataMember]
        public string movimentosair;
        [DataMember]
        public bool flbioentrada;
        [DataMember]
        public bool flbiosaida;
        [DataMember]
        public string identificador;
        [DataMember]
        public int idatuadordgc;
        [DataMember]
        public bool flatualizadatahora;
        [DataMember]
        public bool flatualizaipdevice;
        [DataMember]
        public string ipserverdigicon;
        [DataMember]
        public bool lerstatusdispositivo;
        [DataMember]
        public bool flmodoemergencia;
        [DataMember]
        public int empresaId;
        [DataMember]
        public bool flupdatesummertime;
        [DataMember]
        public int tipoFluxo;

        public string Descricao {
            get { return descricao; }
            set { descricao = value; }
        }
    }

    [DataContract]
    public class Reader {
        [DataMember]
        public int id;
        [DataMember]
        public int codigo;
        [DataMember]
        public int sentido;
        [DataMember]
        public int atuadorId;
        [DataMember]
        public string tipo;
        [DataMember]
        public bool acessoVeicular;
        [DataMember]
        public bool urna;
    }

    [DataContract]
    public class ApiUserMessageObjectResponse {
        [DataMember]
        public long hashCode;
        [DataMember]
        public long id;
        [DataMember]
        public string acessovalido;
        [DataMember]
        public bool ativo;
        [DataMember]
        public string cargoempresa;
        [DataMember]
        public long codUsrgrp;
        [DataMember]
        public long codsala;
        [DataMember]
        public string cpf;
        [DataMember]
        public string digital1;
        [DataMember]
        public string digital2;
        [DataMember]
        public string dptempresa;
        [DataMember]
        public string email1;
        [DataMember]
        public string email2;
        [DataMember]
        public string endresidencial;
        [DataMember]
        public string foto;
        [DataMember]
        public string nomecompleto;
        [DataMember]
        public string nomeexibicao;
        [DataMember]
        public string outrodoc;
        [DataMember]
        public string passwd;
        [DataMember]
        public string rg;
        [DataMember]
        public string sexo;
        [DataMember]
        public string tel1;
        [DataMember]
        public string tel2;
        [DataMember]
        public string tel3;
        [DataMember]
        public bool validausr;
        [DataMember]
        public bool reset;
        [DataMember]
        public string movimento;
        [DataMember]
        public long idperfil;
        [DataMember]
        public bool flmonamb;
        [DataMember]
        public string identificador;
        [DataMember]
        public long empresa_id;
        [DataMember]
        public string numplaca;
        [DataMember]
        public string descricao;
        [DataMember]
        public string nummatricula;
        [DataMember]
        public string tagveicular;
    }

    [DataContract]
    public class ApiUserObjectResponse {
        [DataMember]
        public string codigo;
        [DataMember]
        public ApiUserMessageObjectResponse mensagem;
    }

    [DataContract]
    public class ApiUserResponse : ApiResponse {
        [DataMember]
        public ApiUserObjectResponse objectResponse;
    }

    [DataContract]
    public class ApiResultMessage {
        [DataMember]
        public string codigo;

        [DataMember]
        public string mensagem;
    }

    [DataContract]
    public class ApiResponse : ApiGeneralResponse {

        [DataMember]
        public ApiResultMessage message;
    }

    [DataContract]
    public class ApiResponse2 : ApiGeneralResponse {

        [DataMember]
        public ApiResultMessage2 objectResponse;
    }


    [DataContract]
    public class ApiResultMessage2 {
        [DataMember]
        public string codigo;

        [DataMember]
        public bool mensagem;
    }

    [DataContract]
    public class ApiGeneralResponse {
        [DataMember]
        public bool isRequestValid;
    }

    [DataContract]
    public class ConfigSystem {

        [DataMember]
        public long hashCode;

        [DataMember]
        public long id;

        [DataMember]
        public string accesskey;

        [DataMember]
        public string hourbeginloademp;

        [DataMember]
        public string hourendloademp;

        [DataMember]
        public string timeexecloademp;

        [DataMember]
        public bool flexecloademp;

        [DataMember]
        public string ipserverdgc;

        [DataMember]
        public string pathlibdgc;

        [DataMember]
        public int portserversocket;

        [DataMember]
        public string hourlowautomvisitorbegin;

        [DataMember]
        public string hourlowautomvisitorend;

        [DataMember]
        public string initsummertime;

        [DataMember]
        public string endsummertime;

        [DataMember]
        public string cser;

        [DataMember]
        public bool gestaovaga;

        [DataMember]
        public bool gestaopet;

        [DataMember]
        public bool senhaAcesso;

        [DataMember]
        public string ipMobileServer;
    }

    [DataContract]
    public class ConfigSystemReq : ApiGeneralResponse {
        [DataMember]
        public ConfigSystemObjectResponse objectResponse;
        [DataMember]
        public string message;
    }

    [DataContract]
    public class ConfigSystemObjectResponse {
        [DataMember]
        public string codigo;
        [DataMember]
        public ConfigSystem mensagem;
    }

    [DataContract]
    public class UsersListRequestObj {
        [DataMember]
        public string condicaoValidacao;
        [DataMember]
        public string condicaoWhere;
        [DataMember]
        public string usuario;
    }

    [DataContract]
    public class AtuadorObectAPIResponse {
        [DataMember]
        public string codigo;
        [DataMember]
        public string mensagem;
    }

    [DataContract]
    public class ApiAtuadorResponse : ApiResponse {
        [DataMember]
        public AtuadorObectAPIResponse objectResponse;
    }

    [DataContract]
    public class AtuadoresListObjectResponse {
        [DataMember]
        public string codigo;
        [DataMember]
        public Atuador[] mensagem;
    }

    [DataContract]
    public class ApiListAtuadoresResponse : ApiResponse {
        [DataMember]
        public AtuadoresListObjectResponse objectResponse;
    }

    [DataContract]
    public class Template {

        public static int BIOMETRIA_NORMAL = 0;
        public static int BIOMETRIA_PANICO = 1;

        [DataMember]
        public int id;
        [DataMember]
        public String digital;
        [DataMember]
        public int usuarioId;
        [DataMember]
        public bool ativo;
        [DataMember]
        public int fabricanteId;
        [DataMember]
        public int tipoBiometria;

    }

    [DataContract]
    public class Usuario {

        public List<Template> templates =  new List<Template>();
        public int[] atuadoresIdsList;

        [DataMember]
        public int hashCode = 0;

        // primary key
        [DataMember]
        public int id;

        // fields
        [DataMember]
        public string acessovalido;
        [DataMember]
        public bool ativo;
        [DataMember]
        public string cargoempresa;
        [DataMember]
        public long codUsrgrp;
        [DataMember]
        public long codsala;
        [DataMember]
        public string cpf;
        [DataMember]
        public string digital1;
        [DataMember]
        public string digital2;
        [DataMember]
        public string dptempresa;
        [DataMember]
        public string email1;
        [DataMember]
        public string email2;
        [DataMember]
        public string endresidencial;
        [DataMember]
        public string foto;
        [DataMember]
        public string nomecompleto;
        [DataMember]
        public string nomeexibicao;
        [DataMember]
        public string outrodoc;
        [DataMember]
        public string passwd;
        [DataMember]
        public string rg;
        [DataMember]
        public string sexo;
        [DataMember]
        public string tel1;
        [DataMember]
        public string tel2;
        [DataMember]
        public string tel3;
        [DataMember]
        public bool validausr;
        [DataMember]
        public bool reset;
        [DataMember]
        public string movimento;
        [DataMember]
        public long idperfil;
        [DataMember]
        public bool flmonamb;
        [DataMember]
        public string identificador;
        [DataMember]
        public int empresa_id;
        [DataMember]
        public int idvaga;
        [DataMember]
        public string numplaca;
        [DataMember]
        public string descricao;
        [DataMember]
        public int digitalativa;
        [DataMember]
        public string nummatricula;
        [DataMember]
        public string tagveicular;
        [DataMember]
        public string dataMovimento;
        [DataMember]
        public string horaMovimento;
        [DataMember]
        public string datarefcartprov;
        [DataMember]
        public int motivoId;

        public List<Template> getTemplates(int type) {
            return templates.Where(x => x.tipoBiometria == type).ToList();
        }


    }

    [DataContract]
    public class ApiUsuarioReaderObject
    {

        [DataMember]
        public string idCard;
        [DataMember]
        public string idDevice;
        [DataMember]
        public string dataEvento;
        [DataMember]
        public string horaEvento;
        [DataMember]
        public int readerId;
        [DataMember(EmitDefaultValue = false)]
        public string messages = "";
        [DataMember]
        public string idUsuario = "0";
    }
}

