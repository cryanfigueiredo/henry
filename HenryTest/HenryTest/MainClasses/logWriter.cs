using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace HenryTest.MainClasses{

	public class LogWriter{

		public LogWriter(){

		}


		public void WriteLog(string logMessage = null, bool lAddTime = true) {

            try {

                string FilePath = AppDomain.CurrentDomain.BaseDirectory + "\\HenryKernelLogs.txt";
                FileInfo fi = new FileInfo(FilePath);

                if (!fi.Exists) {
                    File.Create(FilePath).Dispose();
                }

                List<string> lines = File.ReadLines(FilePath).ToList();

                if (lines.Count > 0) {

                    string line = lines.First();

                    if (!String.IsNullOrEmpty(line) && line.Length >= 10) {

                        string[] date = line.Substring(0, 10).Split('/');

                        DateTime date1 = new DateTime(Int32.Parse(date[2]), Int32.Parse(date[1]), Int32.Parse(date[0]));

                        // Depois de uma semana, os logs são apagados...
                        if ((DateTime.Now.Date - date1.Date).TotalDays >= 7) {

                            fi.Delete();
                            File.Create(FilePath).Dispose();

                        }
                    }
                }

                //Console.WriteLine(fi.FullName);
                using (StreamWriter w = fi.AppendText()) {
                    if (logMessage == null)
                        w.WriteLine();
                    else if (lAddTime)
                        w.WriteLine("{0:dd/MM/yyyy HH:mm:ss.fff} {1}", DateTime.Now, logMessage);
                    else
                        w.WriteLine(logMessage);

                    w.Close();
                }
            }
            catch (Exception e) {
				return;
            }
        }

        public static void WriteLog2(string logMessage = null, bool lAddTime = true)
        {

            try
            {

                string FilePath = AppDomain.CurrentDomain.BaseDirectory + "\\HenryKernelLogs.txt";
                FileInfo fi = new FileInfo(FilePath);

                if (!fi.Exists)
                {
                    File.Create(FilePath).Dispose();
                }

                List<string> lines = File.ReadLines(FilePath).ToList();

                if (lines.Count > 0)
                {

                    string line = lines.First();

                    if (!String.IsNullOrEmpty(line) && line.Length >= 10)
                    {

                        string[] date = line.Substring(0, 10).Split('/');

                        DateTime date1 = new DateTime(Int32.Parse(date[2]), Int32.Parse(date[1]), Int32.Parse(date[0]));

                        // Depois de uma semana, os logs são apagados...
                        if ((DateTime.Now.Date - date1.Date).TotalDays >= 7)
                        {

                            fi.Delete();
                            File.Create(FilePath).Dispose();

                        }
                    }
                }

                //Console.WriteLine(fi.FullName);
                using (StreamWriter w = fi.AppendText())
                {
                    if (logMessage == null)
                        w.WriteLine();
                    else if (lAddTime)
                        w.WriteLine("{0:dd/MM/yyyy HH:mm:ss.fff} {1}", DateTime.Now, logMessage);
                    else
                        w.WriteLine(logMessage);

                    w.Close();
                }
            }
            catch (Exception e)
            {
                return;
            }
        }
    }
}