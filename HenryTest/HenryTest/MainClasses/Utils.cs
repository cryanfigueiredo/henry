using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using HenryTest.Henry8X;
using System.Text.RegularExpressions;

namespace HenryTest.MainClasses{

	public class Utils{

        public static bool DeviceIsOnTheNet(String ip)
        {

            bool yes = true;

            try
            {

                Ping myPing = new Ping();
                PingReply reply = myPing.Send(ip, 3000);

                if (reply.Status != IPStatus.Success)
                {
                    yes = false;
                }

            }
            catch (WebException e)
            {
                yes = false;
            }
            catch (NetworkInformationException e)
            {
                yes = false;
            }
            catch (PingException e)
            {
                yes = false;
            }

            return yes;

        }

        public static IPAddress[] GetIpAdresses() {

            System.Net.IPHostEntry host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());

            return host.AddressList.Where((x) => {
                return x.AddressFamily == AddressFamily.InterNetwork
                /*|| x.AddressFamily == AddressFamily.InterNetworkV6*/;
            }).ToArray();
        }

        // Verificação da forma da URL passada
        // para se aceitar apenas o formato: xxx.xxx.xxx.xxx, IPv4
        public static bool IsValidUrl(string url) {

            if (!String.IsNullOrWhiteSpace(url) && !String.IsNullOrEmpty(url)) {

                Regex letters = new Regex(@"[A-Za-z]");

                // Se há letras ou se não tem o comprimento ao menos o correspondente a: 0.0.0.0,
                // Retorna inválida
                if (!letters.IsMatch(url) & url.Length >= 7) {

                    MatchCollection mc = Regex.Matches(url, "\\.");

                    return mc.Count == 3 ? IPv4PointsRightSpreading(url) : false;

                }

                return false;
            }

            return false;
        }

        public static bool IPv4PointsRightSpreading(string url) {

            int i = 0, previous = url.IndexOf(".");

            for (i = 0; i < url.Length; i++) {

                // Se é um ponto "."
                if (String.Compare(url[i].ToString(), ".") == 0) {

                    // Se estão juntos "xx..xx"
                    if (Math.Abs(i - previous) == 1) { return false; }
                    previous = i;
                }

            }

            return true;
        }

        public static ObjetoRequest GetTransformMsgToObj(string msg)
        {
            ObjetoRequest obj = new ObjetoRequest();

            string[] msgSplit = msg.Split('+');
            string[] msgUser = msgSplit[3].Split(']');
            string[] msgDataHora = msgUser[2].Split(' ');

            obj.codigoEvento = msgUser[0];
            obj.matricula = msgUser[1];
            obj.data = msgDataHora[0];
            obj.hora = msgDataHora[1];
            obj.direcao = msgUser[3];
            obj.indicadorDeAcesso = msgUser[4];
            obj.leitoraUtilizada = msgUser[5].Substring(0, 1);


            return obj;

        }

        public static byte[] SendMsg(string msg)
        {
            //01+REON+00+5]7] L i b e r a d o]

            String aux = "", aux2 = "", str = "";
            string BYTE_INIT, BYTE_END;
            uint BYTE_CKSUM;
            string[] BYTE_TAM = new string[2];

            BYTE_INIT = Convert.ToString(2,16);//conf. bit inicial
            BYTE_END = Convert.ToString(3, 16);//conf. bit final
            BYTE_TAM[0] = msg.Length.ToString();//conf. tamanho dos dados
            BYTE_TAM[1] = Convert.ToString(0, 16);
            aux2 += BYTE_INIT; //Inserindo byte inicial
            aux2 += BYTE_TAM[0]; //Inserindo byte do tamanho
            aux2 += BYTE_TAM[1];
            foreach(char chr in msg)
            {
                str += chr;
            }
            aux = aux2 + str; // concatenando com a informacao

            BYTE_CKSUM = aux[1];//Calculo do Checksum
            for (int a = 2; a < aux.Length; a++)
            {
                BYTE_CKSUM = (char)(BYTE_CKSUM ^ aux[a]);
            }
            aux += BYTE_CKSUM; //Inserindo Checksum
            aux += BYTE_END; //Inserindo byte Final
            return Encoding.Unicode.GetBytes(aux);
        }
    }
}