﻿using System;
using System.Windows.Forms;
using HenryTest.TCP_IP;
using HenryTest.Henry8X;

namespace HenryTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConectarClient_Click(object sender, EventArgs e)
        {
            string host = txtHost.Text;
            Int32 port = Convert.ToInt32(txtPort.Text);
            string sendMsg = richSendMsg.Text;

            Client clt = new Client();
            clt.Test(host, port);
            //StatusCommands.ResultStatus(0);

        }

        private void btnConectarServer_Click(object sender, EventArgs e)
        {
            string host = txtHost.Text;
            Int32 port = Convert.ToInt32(txtPort.Text);

            Server.Conectar(host, port);
        }
    }
}
