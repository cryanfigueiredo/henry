﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HenryTest.Henry8X
{
    class StatusCommands
    {
        public enum Status
        {
            NAO_HA_ERRO = 0,
            NAO_HA_DADOS = 1,
            COMANDO_DESCONHECIDO = 10,
            TAMANHO_PACOTE_INVALIDO = 11,
            PARAMETROS_INVALIDOS = 12,
            ERRO_CHECKSUN = 13,
            TAMANHO_PARAMETROS_INVALIDO = 14,
            NUMERO_MENSAGEM_INVALIDO = 15,
            START_BYTE_INVALIDO = 16,
            ERRO_RECEBER_PACOTE = 17,
            NAO_HA_EMPREGADOR_CADASTRADO = 20,
            NAO_HA_USUARIOS_CADASTRADO = 21,
            USUARIO_NAO_CADASTRADO = 22,
            USUARIO_JA_CADASTRADO = 23,
            LIMITE_CADASTRO_ATINGIDO = 24,
            EQUIPAMENTO_SEM_BIOMETRIA = 25,
            INDEX_BIOMETRICO_NAO_ENCONTRADO = 26,
            LIMITE_CADASTRO_DIGITAIS_ATINGIDO = 27,
            EQUIPAMENTO_NAO_POSSUI_EVENTOS = 28,
            ERRO_AO_MANIPULAR_BIOMETRIAS = 29,
            DOCUMENTO_EMPREGADOR_INVALIDO = 30,
            TIPO_DOCUMENTO_EMPREGADOR_INVALIDO = 31,
            IP_INVALIDO = 32,
            TIPO_OPERACAO_USUARIO_INVALIDA = 33,
            IDENTIFICADOR_EMPREGADO_INVALIDO = 34,
            DOCUMENTO_EMPREGADOR_INVALIDO2 = 35,
            REFERENCIA_EMPREGADO_INVALIDA = 36,
            REFERENCIA_CARTAO_USUARIO_INVALIDA = 37,
            ERRO_AO_GRAVAR_DADOS = 43,
            ERRO_AO_LER_DADOS = 44,
            ERRO_DESCONHECIDO = 50,
            MATRICULA_JA_EXISTENTE = 61,
            IDENTIFICADOR_JA_EXISTENTE = 62,
            OPCAO_INVALIDA = 63,
            MATRICULA_INEXISTENTE = 64,
            IDENTIFICADOR_INEXISTENTE = 65,
            CARTAO_NESCESSARIO_NAO_INFORMADO = 66,
            HORARIO_CONTIDO_USUARIO_INEXISTENTE = 180,
            PERIODO_CONTIDO_HORARIO_INEXISTENTE = 181,
            ESCALA_CONTIDO_USUARIO_INEXISTENTE = 182,
            FAIXA_DIASDASEMANA_INVALIDA = 183,
            HORA_NAO_INFORMADA_INVALIDA = 184,
            PERIODO_NAO_INFORMADO_INVALIDO = 185,
            HORARIO_NAO_INFORMADO_INVALIDO_CARTOES = 186,
            INDICE_NAO_INFORMADO_INVALIDO_HORPERACIO = 187,
            DATA_NAO_INFORMADA_INVALIDA = 188,
            MENSAGEM_NAO_INFORMADA = 189,
            ERRO_NA_MEMORIA = 190,
            MENSAGEM_NAO_INFORMADA2 = 191,
            INFORMACAO_TIPO_ACESSO_INVALIDA = 192, 
            INFORMACAO_TIPO_CARTAO_INVALIDA = 193,
            REGISTRO_NAO_ENCONTRADO = 240,
            REGISTRO_JA_EXISTE = 241,
            REGISTRO_NAO_EXISTE = 242,
            LIMITE_ATINGIDO = 243,
            ERRO_NO_TIPO_OPERACAO = 244

        }

        public static string ResultStatus(int status)
        {
            string resp = String.Empty;
            for(int i = 0; i < 244; i++)
            {
                if(status == i)
                {
                    resp = Enum.GetName(typeof(Status), i);
                    return resp;
                }
            }
            return resp;

        }

    }
}
