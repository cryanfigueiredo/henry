﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HenryTest.Henry8X
{
    public class ObjetoRequest
    {
        public string codigoEvento { get; set; }
        public string matricula { get; set; }
        public string data { get; set; }
        public string hora { get; set; }
        public string direcao { get; set; }
        public string indicadorDeAcesso { get; set; }
        public string leitoraUtilizada { get; set; }
    }

}
