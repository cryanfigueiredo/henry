﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HenryTest.TCP_IP
{
    class Client
    {

        TcpClient client;

        public void Test(String server, Int32 port)
        {
            client = new TcpClient(server, port);

            Thread T = new Thread(() => {
                Conectar(server, port);
            });

            T.Start();
        }

        public void Conectar(String server, Int32 port)
        {
            try
            {
                if (client == null || !client.Connected)
                {
                    client = new TcpClient(server, port);
                }
                
                // Translate the passed message into ASCII and store it as a Byte array.
                //Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

                // Get a client stream for reading and writing.
                //  Stream stream = client.GetStream();

                NetworkStream stream = client.GetStream();

                while (client.Connected)
                {

                    if(client.Available > 0 && stream.CanRead)
                    {
                        byte[] myReadBuffer = new byte[1024];
                        StringBuilder myCompleteMessage = new StringBuilder();
                        int numberOfBytesRead = 0;

                        // Incoming message may be larger than the buffer size.
                        do
                        {
                            numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length);

                            string mess = Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead);

                            myCompleteMessage.AppendFormat("{0}", mess);

                            
                        }
                        while (stream.DataAvailable);

                        if(myCompleteMessage.Length > 0)
                        {
                            // Print out the received message to the console.
                            Console.WriteLine("You received the following message : " +
                                                            myCompleteMessage.ToString());
                        }
                    }

                    
                }


                // Send the message to the connected TcpServer. 
                //stream.Write(data, 0, data.Length);

                //Console.WriteLine("Sent: {0}", message);

                //// Receive the TcpServer.response.

                //// Buffer to store the response bytes.
                //data = new Byte[256];

                //// String to store the response ASCII representation.
                //String responseData = String.Empty;

                //// Read the first batch of the TcpServer response bytes.
                //Int32 bytes = stream.Read(data, 0, data.Length);
                //responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                //Console.WriteLine("Received: {0}", responseData);

                // Close everything.
                stream.Close();
                client.Close();
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }

            Console.WriteLine("\n Press Enter to continue...");
            Console.Read();
        }
    }
}
