﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HenryTest.ThreadsTest
{
    class TestThread
    {
        public static void Mein()
        {
            // dispara uma nova thread para executar
            Thread t = new Thread(NovaThread);
            t.Start();

            // Simultaneamente, executa uma tarefa na thread principal
            for (int i = 0; i < 10000; i++) Console.Write("1");
        }

        static void NovaThread()
        {
            for (int i = 0; i < 10000; i++) Console.Write("2");
        }
    }
}
